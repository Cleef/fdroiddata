Categories:Multimedia
License:GPLv3
Author Name:meonwax
Author Email:meonwax@meonwax.de
Web Site:
Source Code:https://github.com/meonwax/soundboard
Issue Tracker:https://github.com/meonwax/soundboard/issues

Auto Name:Soundboard
Summary:Play short sound samples on touch
Description:
Select a sound file from your internal or external storage and add it to the
sounds list. It will be copied to the internal application folder and can be
played on touch. You can delete the original file at any time. The sound sample
will still be available in the application until you decide to delete it.
.

Repo Type:git
Repo:https://github.com/meonwax/soundboard.git

Build:0.9.0,1
    commit=0.9.0
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.9.0
Current Version Code:1
